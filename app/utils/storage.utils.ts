
const WAYPARK_AGENT_TAGS:string = 'WAYPARK-AGENT-TAGS';
const WAYPARK_AGENT_ACCOUNT:string = 'WAYPARK-AGENT-ACCOUNT';
const WAYPARK_AGENT_TOKEN:string = 'WAYPARK-AGENT-TOKEN';

export class StorageUtils {

    static getItem(itemName:string):any {
        return JSON.parse(localStorage.getItem(itemName));
    }
    static hasTags():boolean {
        return !!this.getItem(WAYPARK_AGENT_TAGS);
    }
    static hasToken():boolean {
        return !!this.getItem(WAYPARK_AGENT_TOKEN);
    }
    static getTags():Array<any> {
        if(this.hasTags()) {
            return this.getItem(WAYPARK_AGENT_TAGS);
        }
        return [];
    }
    static setTags(tags:Array<any>) {
        localStorage.setItem(WAYPARK_AGENT_TAGS,JSON.stringify(tags));
    }
    static getToken():string {
        if(this.hasToken()) {
            return this.getItem(WAYPARK_AGENT_TOKEN);
        }
    }
    static setToken(token:string):void {
        localStorage.setItem(WAYPARK_AGENT_TOKEN,JSON.stringify(token));
    }
    static removeToken():void {
        localStorage.removeItem(WAYPARK_AGENT_TOKEN);
    }
    static hasAccount():boolean {
        return !!this.getItem(WAYPARK_AGENT_ACCOUNT);
    }
    static getAccount():any {
        if(this.hasAccount()) {
            return this.getItem(WAYPARK_AGENT_ACCOUNT);
        }
    }
    static setAccount(account:any):void {
        localStorage.setItem(WAYPARK_AGENT_ACCOUNT,JSON.stringify(account));
    }
    static removeAccount():void {
        localStorage.removeItem(WAYPARK_AGENT_ACCOUNT);
    }
}