"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
require('rxjs/add/operator/map');
var config_1 = require('./config');
/*
  Generated class for the Plate provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
var PlateService = (function () {
    function PlateService(http) {
        this.http = http;
        this.headers = new http_1.Headers();
    }
    PlateService.prototype.getPlates = function () {
        var plates = this.http.get(config_1.API_URL + '/api/v1/plates', { headers: this.headers });
        return plates;
    };
    PlateService.prototype.customerPlates = function () {
        var plates = this.http.get(config_1.API_URL + '/api/v1/plates/customer', { headers: this.headers });
        console.log(plates);
        return plates;
    };
    PlateService.prototype.addPlate = function (params) {
        var link = config_1.API_URL + '/api/v1/plates/';
        var headers = this.headers;
        headers.append('Content-Type', 'application/json');
        console.log(params);
        return this.http.post(link, params, { headers: headers })
            .map(function (res) { return res.json(); });
        // .catch(this.handleError());
        // return response;
    };
    PlateService.prototype.removePlate = function (id) {
        var link = config_1.API_URL + '/api/v1/plates/' + id;
        var headers = this.headers;
        console.log(link);
        headers.append('Content-Type', 'application/json');
        return this.http.delete(link, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    PlateService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], PlateService);
    return PlateService;
}());
exports.PlateService = PlateService;
