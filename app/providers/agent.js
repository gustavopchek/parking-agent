"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
require('rxjs/add/operator/map');
var config_1 = require('./config');
var agent_1 = require('../models/agent');
var storage_utils_1 = require('../utils/storage.utils');
// import {Observable} from 'rxjs/Observable';
// import { Subject } from 'rxjs/Subject';
var ionic_angular_1 = require('ionic-angular');
/*
  Generated class for the Agent provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
var AgentService = (function () {
    function AgentService(http, events) {
        this.http = http;
        this.events = events;
        this.headers = new http_1.Headers();
        // this.agentObserver = null;
        // this.obs = Observable.create(observer => {
        //     this.agentObserver= observer;
        // });
        // this.obs = new Subject();
    }
    // public notify():void {
    //   this.agentObserver.next('testcde');
    // }
    AgentService.prototype.getAgent = function () {
        var _this = this;
        var data = storage_utils_1.StorageUtils.getAccount();
        // if(data.id){
        return this.http.get(config_1.API_URL + '/api/v1/agents/' + data.id, { headers: this.headers }).map(function (res) {
            var agentData = res.json();
            // console.log(loginData);
            // let agent:Agent = this.readJwt(loginData.token);
            var agent = new agent_1.Agent(agentData);
            // agent.email = email;
            // agent.password = password;
            console.log('Get Agent Successful', agent);
            // if (rememberMe) {
            // console.log('Remember me: Store user and jwt to local storage');
            storage_utils_1.StorageUtils.setAccount(agent);
            // }
            // this.agentObserver.next(agent);
            _this.events.publish('agent:updated', agent);
            return agent;
        });
        // }
    };
    AgentService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http, ionic_angular_1.Events])
    ], AgentService);
    return AgentService;
}());
exports.AgentService = AgentService;
