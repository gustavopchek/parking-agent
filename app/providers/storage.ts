import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class StorageService {

  private storageObserver: any;
  public storage: any;

  constructor() {
    this.storageObserver= null;

    this.storage= Observable.create(observer => {
        this.storageObserver= observer;
    });
  }

  public yourMethod(): void { 

      // This method changes the value of the storage
      // ...

      // Notify to the subscriptor that the value has changed
      this.storageObserver.next('test');
  }
}