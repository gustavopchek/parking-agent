"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var ionic_angular_1 = require('ionic-angular');
var customer_1 = require('../models/customer');
var storage_utils_1 = require('../utils/storage.utils');
var config_1 = require('./config');
var CONTENT_TYPE_HEADER = 'Content-Type';
var APPLICATION_JSON = 'application/json';
// const BACKEND_URL:string = 'http://demo2726806.mockable.io/authenticate';
var BACKEND_URL = config_1.API_URL + '/api/v1/registrations';
var RegisterService = (function () {
    function RegisterService(http, alertCtrl) {
        this.http = http;
        this.alertCtrl = alertCtrl;
    }
    RegisterService.prototype.register = function (name, phone, cpf, email, password, passwordConfirmation) {
        // if(username.toLowerCase() !== 'admin' || password.toLowerCase() !== 'admin') {
        //     let alert = this.alertCtrl.create({
        //         title: 'Invalid credentials',
        //         subTitle: 'You entered invalid credentials !',
        //         buttons: ['Ok']
        //     });
        //       alert.present();
        //     return Observable.throw(alert);
        // } else {
        var headers = new http_1.Headers();
        headers.append(CONTENT_TYPE_HEADER, APPLICATION_JSON);
        // email = "gustavohpk@yahoo.com";
        // password = "123456";
        // console.log(JSON.stringify({customer:{name: name, phone: phone, cpf: cpf, email: email,password: password, passwordConfirm: passwordConfirmation}}));
        return this.http.post(BACKEND_URL, JSON.stringify({ customer: { name: name, phone: phone, cpf: cpf, email: email, password: password, password_confirmation: passwordConfirmation } }), { headers: headers }).map(function (res) {
            var registerData = res.json();
            // let customer:Customer = this.readJwt(registerData.token);
            var customer = new customer_1.Customer(registerData);
            console.log('Register successful', customer);
            // if (rememberMe) {
            console.log('Remember me: Store user and jwt to local storage');
            storage_utils_1.StorageUtils.setAccount(customer);
            storage_utils_1.StorageUtils.setToken(registerData.token);
            // }
            return customer;
        });
        // }
    };
    RegisterService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http, ionic_angular_1.AlertController])
    ], RegisterService);
    return RegisterService;
}());
exports.RegisterService = RegisterService;
