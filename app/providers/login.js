"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var ionic_angular_1 = require('ionic-angular');
var agent_1 = require('../models/agent');
var storage_utils_1 = require('../utils/storage.utils');
var config_1 = require('./config');
var LoginService = (function () {
    function LoginService(http, alertCtrl) {
        this.http = http;
        this.alertCtrl = alertCtrl;
    }
    LoginService.prototype.login = function (email, password, rememberMe) {
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        console.log(JSON.stringify({ user: { email: email, password: password, type: "agent" } }));
        return this.http.post(config_1.API_URL + '/api/v1/sessions', JSON.stringify({ user: { email: email, password: password, type: "agent" } }), { headers: headers }).map(function (res) {
            var loginData = res.json();
            var agent = new agent_1.Agent(loginData);
            storage_utils_1.StorageUtils.setAccount(agent);
            storage_utils_1.StorageUtils.setToken(loginData.token);
            return agent;
        });
    };
    LoginService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http, ionic_angular_1.AlertController])
    ], LoginService);
    return LoginService;
}());
exports.LoginService = LoginService;
