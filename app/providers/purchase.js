"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
require('rxjs/add/operator/map');
var config_1 = require('./config');
/*
  Generated class for the Purchase provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
var Purchase = (function () {
    function Purchase(http) {
        this.http = http;
        this.headers = new http_1.Headers();
    }
    Purchase.prototype.getPurchases = function () {
        var purchases = this.http.get(config_1.API_URL + '/api/v1/purchases', { headers: this.headers });
        return purchases;
    };
    Purchase.prototype.customerPurchases = function () {
        var purchases = this.http.get(config_1.API_URL + '/api/v1/purchases/customer', { headers: this.headers });
        console.log(purchases);
        return purchases;
    };
    Purchase.prototype.createPurchase = function (params) {
        var link = config_1.API_URL + '/api/v1/purchases/';
        var headers = this.headers;
        headers.append('Content-Type', 'application/json');
        console.log(params);
        return this.http.post(link, params, { headers: headers })
            .map(function (res) { return res.json(); });
        // .catch(this.handleError());
        // return response;
    };
    Purchase = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], Purchase);
    return Purchase;
}());
exports.Purchase = Purchase;
