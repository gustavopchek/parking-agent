import {Injectable,Inject} from '@angular/core';
import {Http,Headers,Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {NavController, AlertController} from 'ionic-angular';
import {Agent} from '../models/agent';
import {TabsPage} from '../pages/tabs/tabs';
import {StorageUtils} from '../utils/storage.utils';
import {API_URL} from './config';

@Injectable()
export class AccountService {
  constructor(private http:Http, public alertCtrl: AlertController) {}
  
  account(name:string, email:string, password:string, passwordConfirmation:string, currentPassword:string):Observable<any> {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let data = StorageUtils.getAccount();
    var agentData;

    if(password.length > 0){
      agentData = JSON.stringify({user:{id: data.id.toString(), name: name, email: email,password: password, password_confirmation: passwordConfirmation, current_password: currentPassword},type: "agent"});
    }
    else{
      agentData = JSON.stringify({user:{id: data.id.toString(), name: name, email: email, current_password: currentPassword},type: "agent"});
    }

    console.log(data.id);
    console.log(agentData);

    return this.http.patch(API_URL + '/api/v1/registrations/'+data.id,agentData,{headers:headers}).map((res:Response) => {

      let accountData:any = res.json();
      let agent:Agent = new Agent(accountData);

      StorageUtils.setAccount(agent);

      return agent;
    });
  }
}