import {Injectable,Inject} from '@angular/core';
import {Http,Headers,Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {NavController, AlertController} from 'ionic-angular';
import {Agent} from '../models/agent';
import {TabsPage} from '../pages/tabs/tabs';
import {StorageUtils} from '../utils/storage.utils';
import {API_URL} from './config';

@Injectable()
export class LoginService {
  constructor(private http:Http, public alertCtrl: AlertController) {}
  
  login(email:string,password:string,rememberMe:boolean):Observable<any> {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    console.log(JSON.stringify({user:{email: email,password: password, type: "agent"}}));
    
    return this.http.post(API_URL + '/api/v1/sessions',JSON.stringify({user:{email: email,password: password, type: "agent"}}),{headers:headers}).map((res:Response) => {

      let loginData:any = res.json();
      let agent:Agent = new Agent(loginData);

      StorageUtils.setAccount(agent);
      StorageUtils.setToken(loginData.token);

      return agent;
    });
  }
}