"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var ionic_angular_1 = require('ionic-angular');
var agent_1 = require('../models/agent');
var storage_utils_1 = require('../utils/storage.utils');
var config_1 = require('./config');
var AccountService = (function () {
    function AccountService(http, alertCtrl) {
        this.http = http;
        this.alertCtrl = alertCtrl;
    }
    AccountService.prototype.account = function (name, email, password, passwordConfirmation, currentPassword) {
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        var data = storage_utils_1.StorageUtils.getAccount();
        var agentData;
        if (password.length > 0) {
            agentData = JSON.stringify({ user: { id: data.id.toString(), name: name, email: email, password: password, password_confirmation: passwordConfirmation, current_password: currentPassword }, type: "agent" });
        }
        else {
            agentData = JSON.stringify({ user: { id: data.id.toString(), name: name, email: email, current_password: currentPassword }, type: "agent" });
        }
        console.log(data.id);
        console.log(agentData);
        return this.http.patch(config_1.API_URL + '/api/v1/registrations/' + data.id, agentData, { headers: headers }).map(function (res) {
            var accountData = res.json();
            var agent = new agent_1.Agent(accountData);
            storage_utils_1.StorageUtils.setAccount(agent);
            return agent;
        });
    };
    AccountService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http, ionic_angular_1.AlertController])
    ], AccountService);
    return AccountService;
}());
exports.AccountService = AccountService;
