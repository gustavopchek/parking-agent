import { Injectable } from '@angular/core';
import { Http, Headers,Response } from '@angular/http';
import 'rxjs/add/operator/map';
import {API_URL} from './config';
import {Agent} from '../models/agent';
import {StorageUtils} from '../utils/storage.utils';
// import {Observable} from 'rxjs/Observable';
// import { Subject } from 'rxjs/Subject';
import { Events } from 'ionic-angular';


/*
  Generated class for the Agent provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class AgentService {

  headers: Headers;
  private agentObserver: any;
  public obs: any;

  constructor(private http: Http, public events: Events) {
    this.headers = new Headers();

    // this.agentObserver = null;

    // this.obs = Observable.create(observer => {
    //     this.agentObserver= observer;
    // });
    // this.obs = new Subject();
  }


  // public notify():void {
  //   this.agentObserver.next('testcde');
  // }

  getAgent() {
    let data = StorageUtils.getAccount();
    // if(data.id){
     return this.http.get(API_URL + '/api/v1/agents/'+data.id, {headers: this.headers}).map((res:Response) => {

          let agentData:any = res.json();
          // console.log(loginData);
          // let agent:Agent = this.readJwt(loginData.token);
          let agent:Agent = new Agent(agentData);
          // agent.email = email;
          // agent.password = password;

          console.log('Get Agent Successful', agent);

          // if (rememberMe) {
              // console.log('Remember me: Store user and jwt to local storage');
              StorageUtils.setAccount(agent);
              
          // }
          // this.agentObserver.next(agent);
          this.events.publish('agent:updated', agent);

          return agent;
      });
    // }
  }

}

