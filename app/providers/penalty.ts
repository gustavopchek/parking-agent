import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import {API_URL} from './config';

/*
  Generated class for the Penalty provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class PenaltyService {

	headers: Headers;

  constructor(private http: Http) {
  	this.headers = new Headers();
  }

  getPenalties() {
    let penalties = this.http.get(API_URL + '/api/v1/penalties', {headers: this.headers});
    return penalties;
  }

  agentPenalties() {
    let penalties = this.http.get(API_URL + '/api/v1/penalties/agent', {headers: this.headers});
    return penalties;
  }

  addPenalty(params){
  	var link = API_URL + '/api/v1/penalties/';
  	var headers = this.headers;
    headers.append('Content-Type', 'application/json');
    
    return this.http.post(link, params, {headers: headers})
    .map(res => res.json());
    // .catch(this.handleError());

    // return response;

  }

  removePenalty(id){
    var link = API_URL + '/api/v1/penalties/'+id;
    var headers = this.headers;
    console.log(link);
    headers.append('Content-Type', 'application/json');
    
    return this.http.delete(link, {headers: headers})
    .map(res => res.json());

  }

}

