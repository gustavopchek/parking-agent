import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {PenaltyService} from '../../providers/penalty';

@Component({
  templateUrl: 'build/pages/penalty/penalty.html',
  providers: [PenaltyService]
})
export class PenaltyPage {

  penalty: PenaltyService;

  constructor(private navCtrl: NavController, private params: NavParams) {
    this.penalty = params.get('penalty');
  }
}
