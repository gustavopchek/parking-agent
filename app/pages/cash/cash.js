"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var ionic_angular_1 = require('ionic-angular');
var purchase_1 = require('../../providers/purchase');
var agent_1 = require('../../providers/agent');
var CashPage = (function () {
    function CashPage(nav, p, agentService, events, alertCtrl) {
        this.nav = nav;
        this.p = p;
        this.agentService = agentService;
        this.events = events;
        this.alertCtrl = alertCtrl;
        this.purchaseParams = { value: 0 };
        this.errors = [];
    }
    CashPage.prototype.purchase = function () {
        // this.agent =  StorageUtils.getAccount();
        // console.log(this.agent);
        // if(this.agent){
        // 	this.purchaseParams.agent_id = this.agent.id;
        // }
        var _this = this;
        var result = false;
        if (this.purchaseParams.value != 10 && this.purchaseParams.value != 20 && this.purchaseParams.value != 30) {
            this.errors = [];
            this.errors.push('Valor incorreto.');
        }
        else {
            this.errors = [];
            var confirm = this.alertCtrl.create({
                title: 'Comprar créditos',
                message: 'Confirmar compra de R$' + this.purchaseParams.value + ',00 em créditos?',
                buttons: [
                    { text: 'Cancelar', role: 'cancel', handler: function () { } },
                    { text: 'Sim', handler: function () {
                            _this.loading = true;
                            var params = JSON.stringify({ purchase: _this.purchaseParams
                            });
                            _this.p.createPurchase(params).subscribe(function (data) {
                                console.log('purchased');
                                result = true;
                                _this.agentService.getAgent().subscribe();
                                _this.events.publish('purchases:updated', data);
                                setTimeout(function () { return _this.loading = false; }, 400);
                                setTimeout(function () { return _this.nav.popToRoot(); }, 500);
                            }, function (err) {
                                _this.loading = false;
                                _this.errors = err.json().errors;
                                console.error(err);
                            }, function () { return null; });
                            confirm.dismiss().then(function () {
                            });
                        } }
                ] });
            confirm.present();
        }
    };
    CashPage = __decorate([
        core_1.Component({
            templateUrl: 'build/pages/cash/cash.html',
            providers: [purchase_1.Purchase, agent_1.AgentService]
        }), 
        __metadata('design:paramtypes', [ionic_angular_1.NavController, purchase_1.Purchase, agent_1.AgentService, ionic_angular_1.Events, ionic_angular_1.AlertController])
    ], CashPage);
    return CashPage;
}());
exports.CashPage = CashPage;
