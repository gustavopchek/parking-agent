import {Component} from '@angular/core';
import {NavController, Events, AlertController} from 'ionic-angular';
import {Purchase} from '../../providers/purchase';
import {AgentService} from '../../providers/agent';
import {StorageUtils} from '../../utils/storage.utils';

@Component({
  templateUrl: 'build/pages/cash/cash.html',
  providers: [Purchase, AgentService]
})
export class CashPage {

	purchaseParams = {value: 0};
	agent: any;
  errors = [];
	public loading;

  constructor(private nav: NavController, private p: Purchase, private agentService: AgentService, public events: Events, private alertCtrl: AlertController) {}

  public purchase() {
  	// this.agent =  StorageUtils.getAccount();
  	// console.log(this.agent);
  	// if(this.agent){
  	// 	this.purchaseParams.agent_id = this.agent.id;
  	// }

    var result = false;

    if(this.purchaseParams.value != 10 && this.purchaseParams.value != 20 && this.purchaseParams.value != 30){
      this.errors = [];
      this.errors.push('Valor incorreto.');
    }else{
      this.errors = [];
      var confirm = this.alertCtrl.create({
        title : 'Comprar créditos',
        message : 'Confirmar compra de R$'+ this.purchaseParams.value +',00 em créditos?',
        buttons: [
          {text: 'Cancelar', role:'cancel', handler : () => {}},
          {text: 'Sim', handler : () => {
            
            this.loading = true;
            var params = JSON.stringify({purchase : 
              this.purchaseParams
            });
            this.p.createPurchase(params).subscribe(
              data => {
                console.log('purchased');
                result = true;
                this.agentService.getAgent().subscribe();
                this.events.publish('purchases:updated', data);
                setTimeout(() => this.loading = false, 400);
                setTimeout(() => this.nav.popToRoot(), 500);
              },
              err => {
                this.loading = false;
                this.errors = err.json().errors;
                console.error(err)
              },
              () => null
            );

            confirm.dismiss().then(() => {
            })
          }}
        ]})
        
      confirm.present();
    }
    
  }
 
}
