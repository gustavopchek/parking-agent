import {Component} from '@angular/core';
import {NavController, Events} from 'ionic-angular';
import {Purchase} from '../../providers/purchase';
import {PurchasePage} from '../purchase/purchase';
import {CashPage} from '../cash/cash';

@Component({
  templateUrl: 'build/pages/purchases/purchases.html',
  providers: [Purchase]
})
export class PurchasesPage {

	public foundPurchases;

  constructor(private navCtrl: NavController, private purchases: Purchase, public events: Events) {
    this.events.subscribe('purchases:updated', (purchasesData) => {
      purchasesData[0].created_at = new Date(purchasesData[0].created_at).toLocaleString([], {day: '2-digit', month: '2-digit', year: '2-digit', hour: '2-digit', minute:'2-digit'});
      this.foundPurchases.unshift(purchasesData[0]);
      // console.log(purchasesData[0]);
    });

  }
	ngOnInit() {
    this.getPurchases();
  }

  getPurchases() {
    this.purchases.customerPurchases().subscribe(
      data => {
        this.foundPurchases = data.json();
        for (var key in this.foundPurchases) {
          this.foundPurchases[key].created_at = new Date(this.foundPurchases[key].created_at).toLocaleString([], {day: '2-digit', month: '2-digit', year: '2-digit', hour: '2-digit', minute:'2-digit'});
        };
        console.log(this.foundPurchases);
      },
      err => console.error(err),
      () => console.log('getPurchases completed')
    );
	}

  openCash(){
    this.navCtrl.push(CashPage);
  }

  openPurchase(purchase) {
    // console.log('purchase open');
    // purchase.created_at = new Date(purchase.created_at).toLocaleString()
    this.navCtrl.push(PurchasePage, {
      purchase: purchase
    });
  };
}
