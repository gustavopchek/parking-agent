"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var ionic_angular_1 = require('ionic-angular');
var purchase_1 = require('../../providers/purchase');
var purchase_2 = require('../purchase/purchase');
var cash_1 = require('../cash/cash');
var PurchasesPage = (function () {
    function PurchasesPage(navCtrl, purchases, events) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.purchases = purchases;
        this.events = events;
        this.events.subscribe('purchases:updated', function (purchasesData) {
            purchasesData[0].created_at = new Date(purchasesData[0].created_at).toLocaleString([], { day: '2-digit', month: '2-digit', year: '2-digit', hour: '2-digit', minute: '2-digit' });
            _this.foundPurchases.unshift(purchasesData[0]);
            // console.log(purchasesData[0]);
        });
    }
    PurchasesPage.prototype.ngOnInit = function () {
        this.getPurchases();
    };
    PurchasesPage.prototype.getPurchases = function () {
        var _this = this;
        this.purchases.customerPurchases().subscribe(function (data) {
            _this.foundPurchases = data.json();
            for (var key in _this.foundPurchases) {
                _this.foundPurchases[key].created_at = new Date(_this.foundPurchases[key].created_at).toLocaleString([], { day: '2-digit', month: '2-digit', year: '2-digit', hour: '2-digit', minute: '2-digit' });
            }
            ;
            console.log(_this.foundPurchases);
        }, function (err) { return console.error(err); }, function () { return console.log('getPurchases completed'); });
    };
    PurchasesPage.prototype.openCash = function () {
        this.navCtrl.push(cash_1.CashPage);
    };
    PurchasesPage.prototype.openPurchase = function (purchase) {
        // console.log('purchase open');
        // purchase.created_at = new Date(purchase.created_at).toLocaleString()
        this.navCtrl.push(purchase_2.PurchasePage, {
            purchase: purchase
        });
    };
    ;
    PurchasesPage = __decorate([
        core_1.Component({
            templateUrl: 'build/pages/purchases/purchases.html',
            providers: [purchase_1.Purchase]
        }), 
        __metadata('design:paramtypes', [ionic_angular_1.NavController, purchase_1.Purchase, ionic_angular_1.Events])
    ], PurchasesPage);
    return PurchasesPage;
}());
exports.PurchasesPage = PurchasesPage;
