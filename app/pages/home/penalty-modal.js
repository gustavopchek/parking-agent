"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var ionic_angular_1 = require('ionic-angular');
var plate_1 = require('../../providers/plate');
var http_1 = require('@angular/http');
var authorization_handler_1 = require('../../utils/authorization-handler');
// import { TextMaskModule } from 'angular2-text-mask';
var PlateModal = (function () {
    function PlateModal(viewCtrl, plateService, alertCtrl) {
        this.viewCtrl = viewCtrl;
        this.plateService = plateService;
        this.alertCtrl = alertCtrl;
        this.plateParams = { code: '' };
        this.errors = [];
        this.mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
    }
    PlateModal.prototype.ngOnInit = function () {
        this.getCustomerPlates();
    };
    PlateModal.prototype.getCustomerPlates = function () {
        var _this = this;
        this.plateService.customerPlates().subscribe(function (data) {
            _this.customerPlates = data.json();
            // for (var key in this.foundActivations) {
            //   this.foundActivations[key].created_at = new Date(this.foundActivations[key].created_at).toLocaleString([], {day: '2-digit', month: '2-digit', year: '2-digit', hour: '2-digit', minute:'2-digit'});
            // };
        }, function (err) { return console.error(err); }, function () { return console.log('getCustomerPlates completed'); });
    };
    PlateModal.prototype.addPlate = function () {
        var _this = this;
        this.loading = true;
        // console.log(params);
        var params = JSON.stringify({ plate: this.plateParams
        });
        this.plateService.addPlate(params).subscribe(function (data) {
            console.log('plate added');
            _this.loading = false;
            // let alert = this.alertCtrl.create({  title : 'Aviso', message : "Placa cadastrada", buttons: ['OK']});
            // alert.present();
            _this.viewCtrl.dismiss(data);
        }, function (err) {
            _this.loading = false;
            _this.errors = [];
            var errors = err.json().errors;
            for (var field in errors) {
                for (var _i = 0, _a = errors[field]; _i < _a.length; _i++) {
                    var item = _a[_i];
                    _this.errors.push(item);
                }
            }
            ;
            console.log(_this.errors);
        }, function () { return console.log('plate added ok'); });
    };
    PlateModal.prototype.deleteWithConfirmation = function () {
    };
    PlateModal.prototype.removePlate = function (id) {
        var _this = this;
        var result = '';
        var confirm = this.alertCtrl.create({
            title: 'Excluir placa',
            message: 'Confirmar exclusão?',
            buttons: [
                { text: 'Cancelar', role: 'cancel', handler: function () { } },
                { text: 'Sim', handler: function () {
                        _this.loading = true;
                        _this.plateService.removePlate(id).subscribe(function (data) {
                            _this.errors = [];
                            _this.loading = false;
                            result = "Placa excluída.";
                            _this.getCustomerPlates();
                            var alert = _this.alertCtrl.create({ title: 'Aviso', message: result, buttons: ['OK'] });
                        }, function (err) {
                            _this.loading = false;
                            result = "Não foi possível excluir a placa.";
                            console.error(err);
                            var alert = _this.alertCtrl.create({ title: 'Aviso', message: result, buttons: ['OK'] });
                        }, function () { return null; });
                        confirm.dismiss().then(function () {
                        });
                    } }
            ] });
        confirm.present();
    };
    PlateModal.prototype.dismiss = function (data) {
        this.viewCtrl.dismiss(data);
    };
    PlateModal = __decorate([
        core_1.Component({
            templateUrl: 'build/pages/home/plate-modal.html',
            providers: [plate_1.PlateService,
                core_1.provide(http_1.Http, {
                    useFactory: function (xhrBackend, requestOptions) {
                        return new authorization_handler_1.AuthorizationHandler(xhrBackend, requestOptions);
                    },
                    deps: [http_1.XHRBackend, http_1.RequestOptions],
                    multi: false
                })
            ]
        }), 
        __metadata('design:paramtypes', [ionic_angular_1.ViewController, plate_1.PlateService, ionic_angular_1.AlertController])
    ], PlateModal);
    return PlateModal;
}());
exports.PlateModal = PlateModal;
