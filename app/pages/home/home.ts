import {Component} from '@angular/core';
import {NavController, ModalController, ViewController, MenuController, NavParams, Events} from 'ionic-angular';
import {Ticket} from '../../providers/ticket/ticket';
import {Activation} from '../../providers/ticket/activation';
import {AgentService} from '../../providers/agent';
import {Agent} from '../../models/agent';
import {PlateService} from '../../providers/plate';
import {PenaltyModal} from './penalty-modal';
import {StorageUtils} from '../../utils/storage.utils';
import {StorageService} from '../../providers/storage';
import {CashPage} from '../cash/cash';

@Component({
  templateUrl: 'build/pages/home/home.html',
  providers: [Ticket, Activation, PlateService, AgentService]
})
export class HomePage {

	foundActivations = [];
	active;
	plate;
  currentActivation = null;
  lastActivation = null;
	connected;
  loading;
  errors = [];
  agentPlates;
  plateParams = {code: ''};
  agent: Agent;
  cash;
	// public lastActivation;

  constructor(private navCtrl: NavController, private menuCtrl: MenuController, private modalCtrl: ModalController, private tickets: Ticket, private activations: Activation, private plateService: PlateService, private agentService: AgentService, public events: Events) {

    this.events.subscribe('agent:updated', (agentData) => {
      this.agent = agentData[0];
    });
  }

  ngOnInit() {
    this.active = false;
    this.connected = true;
    this.loading = false;
    this.menuCtrl.enable(true);
    this.agent = StorageUtils.getAccount();
  }

  openCash(){
    this.navCtrl.push(CashPage);
  }


  submit() {
    this.loading = true;
    // console.log(params);
    // var params = JSON.stringify({activation : 
    //   this.plateParams
    // });
    this.activations.plateActivations(this.plateParams.code).subscribe(
      data => {
        this.foundActivations = data.json();
        // this.active = true;
        this.loading = false;
        // console.log(this.foundActivations[0]);
        if(this.foundActivations.length > 0){
          this.lastActivation = null;
          for (var key in this.foundActivations) {
            this.foundActivations[key].created_at = new Date(this.foundActivations[key].created_at).toLocaleString([], {day: '2-digit', month: '2-digit', year: '2-digit', hour: '2-digit', minute:'2-digit'});
          };
          if(this.foundActivations[0].remaining > 0){
            this.currentActivation = this.foundActivations[0];
            this.lastActivation = null;
          }else{
            this.lastActivation = this.foundActivations[0];
            let remaining = Math.abs(this.lastActivation.remaining);
            let remainingText = "";
            if(remaining > 60){
              remainingText = Math.floor(remaining / 60) + " horas";
            }else if(remaining > 1440){
              remainingText = Math.floor(remaining / 1440) + " dias";
            }else{
              remainingText = remaining + " minutos";
            }
            this.lastActivation.remaining = remainingText;
            this.currentActivation = null;
          }

        }else{
          this.currentActivation = null;
          this.lastActivation = null;
        }
        this.errors = [];
        // this.agentService.getAgent().subscribe();
        // this.events.publish('activations:updated', this.activation);
      },
      err => {
        this.loading = false;
        this.errors = err.json().errors;
        console.log(this.errors);
      },
      () => console.log('activation created')
    );
  }

  createPenalty() {
    let penaltyModal = this.modalCtrl.create(PenaltyModal, {
      code: this.plateParams.code
    });
    penaltyModal.present();
    penaltyModal.onDidDismiss((data: {  }) => {
      // this.getAgentPlates();
    });
  }

}

// @Component({
//   templateUrl: 'build/pages/home/plate-modal.html',
// })
// class Plate {

//  constructor(public viewCtrl: ViewController) {

//  }

//  dismiss() {
//    let data = { 'foo': 'bar' };
//    this.viewCtrl.dismiss(data);
//  }

// }
