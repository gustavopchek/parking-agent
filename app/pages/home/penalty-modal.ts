import {Component, provide} from '@angular/core';
import {NavController, ViewController, AlertController, NavParams, Events} from 'ionic-angular';
import {PenaltyService} from '../../providers/penalty';
import {Http,RequestOptions, XHRBackend} from '@angular/http';
import {AuthorizationHandler} from '../../utils/authorization-handler';
// import { TextMaskModule } from 'angular2-text-mask';

@Component({
  templateUrl: 'build/pages/home/penalty-modal.html',
  providers: [PenaltyService,
    provide(Http,{
      useFactory:(xhrBackend: XHRBackend, requestOptions: RequestOptions) => {
        return new AuthorizationHandler(xhrBackend, requestOptions);
      },
      deps: [XHRBackend, RequestOptions],
      multi:false
    })
  ]
})
export class PenaltyModal {

  penaltyParams = {code: '', description: '', value: ''};
  errors = [];
  loading;
  customerPenaltys;
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];

  constructor(private viewCtrl: ViewController, private penaltyService: PenaltyService, private alertCtrl: AlertController, private params: NavParams, public events: Events) {
    this.penaltyParams.code = params.get('code');
  }

  ngOnInit() {

  }

  addPenalty() {


    var confirm = this.alertCtrl.create({
      title : 'Multa',
      message : 'Aplicar multa?',
      buttons: [
        {text: 'Cancelar', role:'cancel', handler : () => {}},
        {text: 'Sim', handler : () => {
          
          this.loading = true;
          // console.log(params);
          var params = JSON.stringify({
            penalty : this.penaltyParams,
            plate_code: this.penaltyParams.code
          });
          this.penaltyService.addPenalty(params).subscribe(
            data => {
              console.log('penalty added');
              this.loading = false;
              this.events.publish('penalties:updated', data);
              let alert = this.alertCtrl.create({  title : 'Aviso', message : "Multa aplicada.", buttons: ['OK']});
              alert.present();
              // this.viewCtrl.dismiss(data);
            },
            err => {
              this.loading = false;
              this.errors = [];
                var errors = err.json().errors;
                for (var field in errors) {  
                  for(var item of errors[field]){
                    this.errors.push(item);
                  }
                };
                console.log(this.errors);
            },
            () => console.log('penalty added ok')
          );

          confirm.dismiss().then(() => {

          })
        }}
      ]})
      
    confirm.present();

  }

  deleteWithConfirmation(){
    
  }

  removePenalty(id) {

    var result = '';

    var confirm = this.alertCtrl.create({
      title : 'Excluir placa',
      message : 'Confirmar exclusão?',
      buttons: [
        {text: 'Cancelar', role:'cancel', handler : () => {}},
        {text: 'Sim', handler : () => {
          
          this.loading = true;
          this.penaltyService.removePenalty(id).subscribe(
            data => {
              this.errors = [];
              this.loading = false;
              result = "Placa excluída.";
              let alert = this.alertCtrl.create({  title : 'Aviso', message : result, buttons: ['OK']});
            },
            err => {
              this.loading = false;
              result = "Não foi possível excluir a placa."
              console.error(err);
              let alert = this.alertCtrl.create({  title : 'Aviso', message : result, buttons: ['OK']});
            },
            () => null
          );

          confirm.dismiss().then(() => {

          })
        }}
      ]})
      
    confirm.present();

  }


  dismiss(data) {
    this.viewCtrl.dismiss(data);
  }

}