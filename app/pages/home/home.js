"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var ionic_angular_1 = require('ionic-angular');
var ticket_1 = require('../../providers/ticket/ticket');
var activation_1 = require('../../providers/ticket/activation');
var agent_1 = require('../../providers/agent');
var plate_1 = require('../../providers/plate');
var storage_utils_1 = require('../../utils/storage.utils');
var cash_1 = require('../cash/cash');
var HomePage = (function () {
    // public lastActivation;
    function HomePage(navCtrl, menuCtrl, modalCtrl, tickets, activations, plateService, agentService, events) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.modalCtrl = modalCtrl;
        this.tickets = tickets;
        this.activations = activations;
        this.plateService = plateService;
        this.agentService = agentService;
        this.events = events;
        this.errors = [];
        this.plateParams = { code: '' };
        this.events.subscribe('agent:updated', function (agentData) {
            _this.agent = agentData[0];
        });
    }
    HomePage.prototype.ngOnInit = function () {
        this.getTickets();
        this.active = false;
        this.connected = true;
        this.loading = false;
        this.menuCtrl.enable(true);
        this.agent = storage_utils_1.StorageUtils.getAccount();
    };
    HomePage.prototype.openCash = function () {
        this.navCtrl.push(cash_1.CashPage);
    };
    HomePage.prototype.getTickets = function () {
        var _this = this;
        this.tickets.getTickets().subscribe(function (data) {
            _this.foundTickets = data.json();
        }, function (err) { return console.error(err); }, function () { return console.log('getTickets completed'); });
    };
    HomePage.prototype.submit = function () {
        var _this = this;
        this.loading = true;
        // console.log(params);
        var params = JSON.stringify({ activation: this.plateParams
        });
        this.activations.createActivation(params).subscribe(function (data) {
            console.log('created');
            _this.activation = data;
            _this.active = true;
            _this.loading = false;
            console.log(_this.activation);
            _this.errors = [];
            _this.agentService.getAgent().subscribe();
            _this.events.publish('activations:updated', _this.activation);
        }, function (err) {
            _this.loading = false;
            _this.errors = err.json().errors;
            console.log(_this.errors);
        }, function () { return console.log('activation created'); });
    };
    HomePage = __decorate([
        core_1.Component({
            templateUrl: 'build/pages/home/home.html',
            providers: [ticket_1.Ticket, activation_1.Activation, plate_1.PlateService, agent_1.AgentService]
        }), 
        __metadata('design:paramtypes', [ionic_angular_1.NavController, ionic_angular_1.MenuController, ionic_angular_1.ModalController, ticket_1.Ticket, activation_1.Activation, plate_1.PlateService, agent_1.AgentService, ionic_angular_1.Events])
    ], HomePage);
    return HomePage;
}());
exports.HomePage = HomePage;
// @Component({
//   templateUrl: 'build/pages/home/plate-modal.html',
// })
// class Plate {
//  constructor(public viewCtrl: ViewController) {
//  }
//  dismiss() {
//    let data = { 'foo': 'bar' };
//    this.viewCtrl.dismiss(data);
//  }
// }
