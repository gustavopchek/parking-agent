"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var ionic_angular_1 = require('ionic-angular');
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
// import {TranslatePipe, TranslateService} from 'ng2-translate/ng2-translate';
var register_1 = require('../../providers/register');
var customer_1 = require('../../providers/customer');
var tabs_1 = require('../tabs/tabs');
var login_1 = require('../login/login');
var RegisterPage = (function () {
    function RegisterPage(nav, fb, registerService, customerService) {
        this.nav = nav;
        this.fb = fb;
        this.registerService = registerService;
        this.customerService = customerService;
        this.errors = [];
        this.loading = false;
        this.registerForm = fb.group({
            name: ['', forms_1.Validators.compose([forms_1.Validators.required])],
            phone: ['', forms_1.Validators.compose([forms_1.Validators.required])],
            cpf: ['', forms_1.Validators.compose([forms_1.Validators.required])],
            email: ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(4)])],
            password: ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(6)])],
            passwordConfirmation: ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(6)])]
        });
        this.username = this.registerForm.controls['username'];
        this.password = this.registerForm.controls['password'];
    }
    RegisterPage.prototype.loginPage = function () {
        // this.nav.push(LoginPage);
        this.nav.setRoot(login_1.LoginPage);
    };
    RegisterPage.prototype.register = function (formData) {
        var _this = this;
        // This will be called when the user clicks on the Login button
        this.loading = true;
        console.log(formData);
        // console.log(password);
        this.registerService.register(formData.name, formData.phone, formData.cpf, formData.email, formData.password, formData.passwordConfirmation).subscribe(function (data) {
            _this.loading = false;
            // this.events.publish('customer:updated', customer);
            _this.customerService.getCustomer().subscribe();
            _this.nav.setRoot(tabs_1.TabsPage);
        }, function (err) {
            // console.log(err.json());
            _this.loading = false;
            _this.errors = [];
            var errors = err.json().errors;
            // err.json().errors.forEach((errors, field) => {
            //   // this.fb.valid = false;
            //   this.errors[field] = errors.join(', ');
            // });
            // console.log(err.json().errors);
            for (var field in errors) {
                //   // this.fb.valid = false;
                for (var _i = 0, _a = errors[field]; _i < _a.length; _i++) {
                    var item = _a[_i];
                    _this.errors.push(item);
                }
            }
            ;
            // this.errors.push("aaa");
            // this.errors.push("aaa");
            console.log(_this.errors);
        }, function () { return console.log('login'); });
    };
    RegisterPage = __decorate([
        core_1.Component({
            selector: 'register-page',
            templateUrl: 'build/pages/register/register.html',
            providers: [register_1.RegisterService]
        }), 
        __metadata('design:paramtypes', [ionic_angular_1.NavController, forms_1.FormBuilder, register_1.RegisterService, (typeof (_a = typeof customer_1.CustomerService !== 'undefined' && customer_1.CustomerService) === 'function' && _a) || Object])
    ], RegisterPage);
    return RegisterPage;
    var _a;
}());
exports.RegisterPage = RegisterPage;
