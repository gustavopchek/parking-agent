"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var ionic_angular_1 = require('ionic-angular');
var penalty_1 = require('../../providers/penalty');
var penalty_2 = require('../penalty/penalty');
var PenaltiesPage = (function () {
    function PenaltiesPage(navCtrl, penalties) {
        this.navCtrl = navCtrl;
        this.penalties = penalties;
    }
    PenaltiesPage.prototype.ngOnInit = function () {
        this.getPenalties();
        // setInterval(() => this.getCustomerPenalties(), 60000);
    };
    PenaltiesPage.prototype.getPenalties = function () {
        var _this = this;
        this.penalties.platePenalties().subscribe(function (data) {
            _this.foundPenalties = data.json();
            for (var key in _this.foundPenalties) {
                _this.foundPenalties[key].created_at = new Date(_this.foundPenalties[key].created_at).toLocaleString([], { day: '2-digit', month: '2-digit', year: '2-digit', hour: '2-digit', minute: '2-digit' });
            }
            ;
        }, function (err) { return console.error(err); }, function () { return console.log('getPenalties completed'); });
    };
    PenaltiesPage.prototype.openPenalty = function (penalty) {
        // console.log('penalty open');
        // penalty.created_at = new Date(penalty.created_at).toLocaleString()
        this.navCtrl.push(penalty_2.PenaltyPage, {
            penalty: penalty
        });
    };
    ;
    PenaltiesPage = __decorate([
        core_1.Component({
            templateUrl: 'build/pages/penalties/penalties.html',
            providers: [penalty_1.Penalty]
        }), 
        __metadata('design:paramtypes', [ionic_angular_1.NavController, penalty_1.Penalty])
    ], PenaltiesPage);
    return PenaltiesPage;
}());
exports.PenaltiesPage = PenaltiesPage;
