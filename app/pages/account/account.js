"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var ionic_angular_1 = require('ionic-angular');
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
// import {TranslatePipe, TranslateService} from 'ng2-translate/ng2-translate';
var account_1 = require('../../providers/account');
var agent_1 = require('../../providers/agent');
var storage_utils_1 = require('../../utils/storage.utils');
var AccountPage = (function () {
    function AccountPage(nav, fb, accountService, agentService) {
        this.nav = nav;
        this.fb = fb;
        this.accountService = accountService;
        this.agentService = agentService;
        this.errors = [];
        this.loading = false;
        this.agent = storage_utils_1.StorageUtils.getAccount();
        console.log(this.agent);
        this.accountForm = fb.group({
            name: [this.agent.name, forms_1.Validators.compose([forms_1.Validators.required])],
            phone: [this.agent.phone, forms_1.Validators.compose([forms_1.Validators.required])],
            cpf: [this.agent.cpf, forms_1.Validators.compose([forms_1.Validators.required])],
            email: [this.agent.email, forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(6)])],
            password: ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(6)])],
            passwordConfirmation: ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(6)])],
            currentPassword: ['', forms_1.Validators.compose([forms_1.Validators.required])],
        });
        this.username = this.accountForm.controls['username'];
        this.password = this.accountForm.controls['password'];
    }
    AccountPage.prototype.account = function (formData) {
        var _this = this;
        this.loading = true;
        this.accountService.account(formData.name, formData.email, formData.password, formData.passwordConfirmation, formData.currentPassword).subscribe(function (data) {
            _this.loading = false;
            _this.agentService.getAgent().subscribe();
            _this.nav.popToRoot();
        }, function (err) {
            _this.loading = false;
            _this.errors = [];
            var errors = err.json().errors;
            for (var field in errors) {
                for (var _i = 0, _a = errors[field]; _i < _a.length; _i++) {
                    var item = _a[_i];
                    _this.errors.push(item);
                }
            }
            ;
            console.log(_this.errors);
        }, function () { return console.log('login'); });
    };
    AccountPage = __decorate([
        core_1.Component({
            templateUrl: 'build/pages/account/account.html',
            providers: [account_1.AccountService]
        }), 
        __metadata('design:paramtypes', [ionic_angular_1.NavController, forms_1.FormBuilder, account_1.AccountService, agent_1.AgentService])
    ], AccountPage);
    return AccountPage;
}());
exports.AccountPage = AccountPage;
