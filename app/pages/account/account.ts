

import {Page, NavController, Alert} from 'ionic-angular';
import {Inject, Component} from '@angular/core';
import {FormBuilder, Validators, FormGroup, AbstractControl} from '@angular/forms';
import {Agent} from '../../models/agent';
// import {TranslatePipe, TranslateService} from 'ng2-translate/ng2-translate';
import {AccountService} from '../../providers/account';
import {AgentService} from '../../providers/agent';
import {TabsPage} from '../tabs/tabs';
import {Response} from '@angular/http';
import {StorageUtils} from '../../utils/storage.utils';
import {LoginPage} from '../login/login';

@Component({
    templateUrl: 'build/pages/account/account.html',
    providers:[AccountService]
})
export class AccountPage {

  accountForm: FormGroup;
  username: AbstractControl;
  password: AbstractControl;
  errors = [];
  loading = false;
  agent: any;

  constructor(private nav: NavController, private fb: FormBuilder, private accountService:AccountService, private agentService: AgentService) {
    this.agent =  StorageUtils.getAccount();
    console.log(this.agent);
    this.accountForm = fb.group({  
        name: [this.agent.name, Validators.compose([Validators.required])],
        phone: [this.agent.phone, Validators.compose([Validators.required])],
        cpf: [this.agent.cpf, Validators.compose([Validators.required])],
        email: [this.agent.email, Validators.compose([Validators.required, Validators.minLength(6)])],
        password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
        passwordConfirmation: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
        currentPassword: ['', Validators.compose([Validators.required])],
    });

    this.username = this.accountForm.controls['username'];     
    this.password = this.accountForm.controls['password'];
  }


  account(formData):void {
    this.loading = true;

    this.accountService.account(formData.name, formData.email, formData.password, formData.passwordConfirmation, formData.currentPassword).subscribe(
      data => {
        this.loading = false;
        this.agentService.getAgent().subscribe();
        this.nav.popToRoot();
      },
      err => {
        this.loading = false;
        this.errors = [];
        var errors = err.json().errors;
        for (var field in errors) {  
          for(var item of errors[field]){
            this.errors.push(item);
          }
        };
        console.log(this.errors);
      },
    () => console.log('login')
    );
  }
}