import {Component} from '@angular/core';
import {NavController, Events} from 'ionic-angular';
import {Activation} from '../../providers/ticket/activation';
import {ActivationPage} from '../activation/activation';

@Component({
  templateUrl: 'build/pages/activations/activations.html',
  providers: [Activation]
})
export class ActivationsPage {

	public foundActivations;

  constructor(private navCtrl: NavController, private activations: Activation, public events: Events) {
    this.events.subscribe('activations:updated', (activationsData) => {
      activationsData[0].created_at = new Date(activationsData[0].created_at).toLocaleString([], {day: '2-digit', month: '2-digit', year: '2-digit', hour: '2-digit', minute:'2-digit'});
      this.foundActivations.unshift(activationsData[0]);
      // console.log(activationsData[0]);
    });
  }
	ngOnInit() {
    this.getActivations();
  }

  getActivations() {

	}

  openActivation(activation) {
    console.log('activation open');
    // activation.created_at = new Date(activation.created_at).toLocaleString()
    this.navCtrl.push(ActivationPage, {
      activation: activation
    });
  };
}
