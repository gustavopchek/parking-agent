"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var ionic_angular_1 = require('ionic-angular');
var activation_1 = require('../../providers/ticket/activation');
var activation_2 = require('../activation/activation');
var ActivationsPage = (function () {
    function ActivationsPage(navCtrl, activations, events) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.activations = activations;
        this.events = events;
        this.events.subscribe('activations:updated', function (activationsData) {
            activationsData[0].created_at = new Date(activationsData[0].created_at).toLocaleString([], { day: '2-digit', month: '2-digit', year: '2-digit', hour: '2-digit', minute: '2-digit' });
            _this.foundActivations.unshift(activationsData[0]);
            // console.log(activationsData[0]);
        });
    }
    ActivationsPage.prototype.ngOnInit = function () {
        this.getActivations();
    };
    ActivationsPage.prototype.getActivations = function () {
        var _this = this;
        this.activations.customerActivations().subscribe(function (data) {
            _this.foundActivations = data.json();
            for (var key in _this.foundActivations) {
                _this.foundActivations[key].created_at = new Date(_this.foundActivations[key].created_at).toLocaleString([], { day: '2-digit', month: '2-digit', year: '2-digit', hour: '2-digit', minute: '2-digit' });
            }
            ;
        }, function (err) { return console.error(err); }, function () { return console.log('getActivations completed'); });
    };
    ActivationsPage.prototype.openActivation = function (activation) {
        console.log('activation open');
        // activation.created_at = new Date(activation.created_at).toLocaleString()
        this.navCtrl.push(activation_2.ActivationPage, {
            activation: activation
        });
    };
    ;
    ActivationsPage = __decorate([
        core_1.Component({
            templateUrl: 'build/pages/activations/activations.html',
            providers: [activation_1.Activation]
        }), 
        __metadata('design:paramtypes', [ionic_angular_1.NavController, activation_1.Activation, ionic_angular_1.Events])
    ], ActivationsPage);
    return ActivationsPage;
}());
exports.ActivationsPage = ActivationsPage;
