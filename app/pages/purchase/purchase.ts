import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {Purchase} from '../../providers/purchase';

@Component({
  templateUrl: 'build/pages/purchase/purchase.html',
  providers: [Purchase]
})
export class PurchasePage {

  purchase: Purchase;
  customerPurchases;

  constructor(private navCtrl: NavController, private params: NavParams) {
    this.purchase = params.get('purchase');
  }

  getCustomerPurchases() {
    this.purchase.customerPurchases().subscribe(
      data => {
        this.customerPurchases = data.json();
      },
      err => console.error(err),
      () => console.log('getCustomerPurchases completed')
    );
  }

}
