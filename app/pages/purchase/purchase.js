"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var ionic_angular_1 = require('ionic-angular');
var purchase_1 = require('../../providers/purchase');
var PurchasePage = (function () {
    function PurchasePage(navCtrl, params) {
        this.navCtrl = navCtrl;
        this.params = params;
        this.purchase = params.get('purchase');
    }
    PurchasePage.prototype.getCustomerPurchases = function () {
        var _this = this;
        this.purchase.customerPurchases().subscribe(function (data) {
            _this.customerPurchases = data.json();
        }, function (err) { return console.error(err); }, function () { return console.log('getCustomerPurchases completed'); });
    };
    PurchasePage = __decorate([
        core_1.Component({
            templateUrl: 'build/pages/purchase/purchase.html',
            providers: [purchase_1.Purchase]
        }), 
        __metadata('design:paramtypes', [ionic_angular_1.NavController, ionic_angular_1.NavParams])
    ], PurchasePage);
    return PurchasePage;
}());
exports.PurchasePage = PurchasePage;
