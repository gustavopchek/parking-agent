"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var ionic_angular_1 = require('ionic-angular');
var forms_1 = require('@angular/forms');
var tabs_1 = require('../tabs/tabs');
var login_1 = require('../../providers/login');
var LoginPage = (function () {
    function LoginPage(form, nav, loginService) {
        this.nav = nav;
        this.loginService = loginService;
        this.rememberMe = false;
        this.errors = [];
        this.loading = false;
        this.loginForm = form.group({
            username: ['', forms_1.Validators.required],
            password: ['', forms_1.Validators.required],
            rememberMe: ['', forms_1.Validators.required]
        });
    }
    LoginPage.prototype.login = function (formData) {
        var _this = this;
        this.loading = true;
        this.loginService.login(formData.username, formData.password, formData.rememberMe).subscribe(function (data) {
            _this.loading = false;
            _this.nav.setRoot(tabs_1.TabsPage);
        }, function (err) {
            _this.loading = false;
            _this.errors = err.json().errors;
            console.log(_this.errors);
        }, function () { return console.log('login'); });
    };
    LoginPage = __decorate([
        ionic_angular_1.Page({
            selector: 'login-page',
            templateUrl: 'build/pages/login/login.html',
            providers: [login_1.LoginService]
        }), 
        __metadata('design:paramtypes', [forms_1.FormBuilder, ionic_angular_1.NavController, login_1.LoginService])
    ], LoginPage);
    return LoginPage;
}());
exports.LoginPage = LoginPage;
