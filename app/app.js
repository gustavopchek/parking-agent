"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var ionic_angular_1 = require('ionic-angular');
var http_1 = require('@angular/http');
var ionic_native_1 = require('ionic-native');
var login_1 = require('./pages/login/login');
var tabs_1 = require('./pages/tabs/tabs');
var about_1 = require('./pages/about/about');
var contact_1 = require('./pages/contact/contact');
var account_1 = require('./pages/account/account');
var storage_utils_1 = require('./utils/storage.utils');
var login_2 = require('./providers/login');
var authorization_handler_1 = require('./utils/authorization-handler');
var agent_1 = require('./providers/agent');
var WayPark = (function () {
    function WayPark(platform, menu, loginService, ngZone, events) {
        var _this = this;
        this.platform = platform;
        this.menu = menu;
        this.loginService = loginService;
        this.ngZone = ngZone;
        this.events = events;
        this.events.subscribe('agent:updated', function (agentData) {
            _this.agent = agentData[0];
        });
        this.menu = menu;
        this.pages = [
            { title: 'Sobre', component: about_1.AboutPage, icon: 'information-circle' },
            { title: 'Contato', component: contact_1.ContactPage, icon: 'contacts' },
            { title: 'Minha conta', component: account_1.AccountPage, icon: 'contact' }
        ];
        if (storage_utils_1.StorageUtils.hasAccount()) {
            this.rootPage = tabs_1.TabsPage;
        }
        else {
            this.rootPage = login_1.LoginPage;
        }
        platform.ready().then(function () {
            ionic_native_1.StatusBar.styleDefault();
        });
    }
    WayPark.prototype.ngOnInit = function () {
        this.agent = storage_utils_1.StorageUtils.getAccount();
    };
    WayPark.prototype.openPage = function (page) {
        this.menu.close();
        this.nav.push(page.component);
    };
    ;
    WayPark.prototype.logout = function () {
        storage_utils_1.StorageUtils.removeToken();
        storage_utils_1.StorageUtils.removeAccount();
        this.menu.enable(false);
        this.nav.setRoot(login_1.LoginPage);
    };
    __decorate([
        core_1.ViewChild('nav'), 
        __metadata('design:type', ionic_angular_1.NavController)
    ], WayPark.prototype, "nav", void 0);
    WayPark = __decorate([
        core_1.Component({
            templateUrl: 'build/app.html',
            providers: [ionic_angular_1.NavController, login_2.LoginService, agent_1.AgentService,
                core_1.provide(http_1.Http, {
                    useFactory: function (xhrBackend, requestOptions) {
                        return new authorization_handler_1.AuthorizationHandler(xhrBackend, requestOptions);
                    },
                    deps: [http_1.XHRBackend, http_1.RequestOptions],
                    multi: false
                })
            ]
        }), 
        __metadata('design:paramtypes', [ionic_angular_1.Platform, ionic_angular_1.MenuController, login_2.LoginService, core_1.NgZone, ionic_angular_1.Events])
    ], WayPark);
    return WayPark;
}());
exports.WayPark = WayPark;
ionic_angular_1.ionicBootstrap(WayPark);
