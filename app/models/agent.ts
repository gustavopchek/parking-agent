export class Agent {
    id:number;
    name:string;
    phone:string;
    cpf:string;
    email:string;
    password:string;
    cash:number;
    token: string;
    rememberMe:boolean;
    constructor(agent?:{id: number, name: string, email: string, token: string}) {
        this.rememberMe = false;
        if(agent) {
            this.id = agent.id;
            this.name = agent.name;
            this.email = agent.email;
        }
    }
}