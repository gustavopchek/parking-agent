"use strict";
var Agent = (function () {
    function Agent(agent) {
        this.rememberMe = false;
        if (agent) {
            this.id = agent.id;
            this.name = agent.name;
            this.email = agent.email;
        }
    }
    return Agent;
}());
exports.Agent = Agent;
